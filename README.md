# Electronic target
Web App using Express.js, Vue.js and Socket.io to simulate an electronic target and its display.

## test on Development build

## run the server
1. go to /server/
2. run `npm install`
3. run `npm run start`
4. localhost:1234 should be running

## vuejs
1. in a different terminal, go to /
2. run `npm install`
3. run `npm run serve`
4. you can test the app using http://localhost:8080/
