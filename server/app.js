
const express = require('express');
const app = express();
const path = require('path');
const coordinate = require("./helpers/coordinate");

const server = app.listen(1234, function() {
    console.log('server running on port 1234');
    setInterval(() => {
        let coordinateData = {
            coordinates : {
                x: coordinate.getRandomCoordinate(),
                y: coordinate.getRandomCoordinate(),
            },
        };
        io.sockets.emit('receive-coordinate', coordinateData);
    }, 1000);
});

const io = require('socket.io')(server, {
    cors: {
        origins: "http://localhost:* http://127.0.0.1:*",
    }
});

app.get('/', (req,res) => {
    res.sendFile(path.join(__dirname, '../public/index.html'));
});

io.on('connection', function(socket) {
    console.log('new user connected', socket.id);
});