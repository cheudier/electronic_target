function getRandomCoordinate() {
    // generate a random point from 0 to 10
    let coordinate = Math.random() * 10;

    // randomly define if it will be positive or negative
    coordinate *= Math.random() > 0.5 ? 1 : -1;

    // only 2 digits after
    coordinate = coordinate.toFixed(2);

    return coordinate;
}

module.exports = { getRandomCoordinate };